﻿<?php 
	$pageKeywords='software engineering support, software specifiaction, software verification, software validation, quality assurance processes, testing tools selection, software testing consulting, testing automation, testing integration in SCRUM, software documenting';
	$pageTitle = 'γ-Test: Support in software engineering';
	$pageDescription = 'γ-Test provides a support for software development processes including specification, validation, verification, documenting, testing and testing automation';
	include ($_SERVER['DOCUMENT_ROOT']."/header.php");
?>

<!-- CONTENT -->
<div class="row pagecontent">
<div class="content box col-md-12">
<br/>

    <div class="col-md-4 col-sm-12 pull-left">
        <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="Software specification" />Specification</h2>
        <p class="thumb"><img src="tmp/software_specification.jpg" alt="Software specification vs requirements" /></a></p>
        <p><h5>Are you having bottle-necks in software development due to lack of specification? Would you like to have clear and complete specification about software you developing? <a href='/contact.php'>Contact us</a> and we will help you to analyze and specify software requirements for your development team.</h5></p>
    </div>

    <div class="col-md-4 col-sm-12 pull-left">
        <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="Validation of software" />Validation</h2>
        <p class="thumb"><img src="tmp/software-validation-and-verification.jpg" alt="Software validation" /></a></p>
        <p><h5>Are you building a right software? Are you solution is enough simple and transparent for the end-user? <a href='/contact.php'>Ask our software analytics</a> to help you build a software that maximally satisfies market and potential customer requirements.</h5></p>
    </div>

    <div class="col-md-4 col-sm-12 pull-left">
        <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="Software documentation" />Documenting</h2>
        <p class="thumb"><img src="tmp/software_documentation.jpg" alt="Software documentation" /></a></p>
        <p><h5>Are you having enough documentation about developed software? Are the environment and construction principles clear for all members of your team? <a href='/contact.php'>Get in touch with <strong>γ-Test</strong></a> if you want anybody clearly understand how you software works!</h5></p>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-4 col-sm-12 pull-left">
        <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="Software testing process organising" />Quality Assurance from scratch</h2>
        <p class="thumb"><img src="tmp/software_testing_organising.jpg" alt="Software testing process organising" /></a></p>
        <p><h5>Are you thinking about Quality Assurance and Quality Control integration? Are you searching the way to develop more qualitable software? Just <a href='/contact.php'>ask <strong>γ-Test</strong> QA and QC engineers</a> and receive a testing plan less than in 48 hours.</h5></p>
    </div>

    <div class="col-md-4 col-sm-12 pull-left">
        <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="Testing tool selection" />Tools selections</h2>
        <p class="thumb"><img src="tmp/testing_tools_selection.jpg" alt="Testing tool selection" /></a></p>

        <p><h5>Are you doubting what kind of tool to use in your software development cycle? Would you like to have a turn-key solution?  <a href='/contact.php'>Ask us for small consultation</a> and we'll save you hundreds of hours and thousands of euros.</h5></p>
    </div>

    <div class="col-md-4 col-sm-12">
        <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="Test automation solutions" />Testing automation</h2>
        <p class="thumb"><img src="tmp/test_automation.jpg" alt="Test automation solutions" /></a></p>
        <p><h5>Are you having a big regression test set needs to be manually executed every week? Would you like to automate some manual tests? <strong>γ-Test</strong> engineers will help you to establish an automation testing process in your company.</h5></p>
    </div>

	</div>
 </div>

	<?php 
	include ($_SERVER['DOCUMENT_ROOT']."/footer.php");
?>

</div> 

</div> 

<!--<script type="text/javascript"> Cufon.now(); </script>-->

</body>
</html>
