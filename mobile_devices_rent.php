﻿<?php
$pageKeywords = 'mobile devices rent, android devices rent, ios devices rent, windows phone devices rent, ipad rent, Estonia, Tallinn';
$pageTitle = 'γ-Test: Mobile devices rent';
$pageDescription = 'γ-Test - Rent a mobile device or special toolkit to test you app on 5-10 real devices by the price of one';
include($_SERVER['DOCUMENT_ROOT'] . "/header.php");
?>
<div class="row pagecontent">
    <div class="content box col-md-12">
        <br>

        <div class="col-md-12 col-sm-12">
            <h2 class="cufon" align="center">Why to buy device if you need it only for short period?</h2>

            <h5 class="cufon">Especially for mobile application developers Gamma Test provides a mobile devices rent for
                software testing. Even more! We are giving devices just in configuration you need, including
                performance, OS
                version and additional features (network support etc.)</h5>

            <h4 class="cufon" align="left" style="margin-top: 10px;">
                <img src="tmp/green_tick_50_50.jpg" alt="We test it"/> Are you looking for device with old OS version?
                <br>
                <img src="tmp/green_tick_50_50.jpg" alt="We test it"/> Do you doubt that your app will work on slow
                device?

            </h4>

            <h5 class="cufon"  style="margin-top: 20px;font-size: 20px !important;color: rgb(32, 32, 32);" align="center">Rent a mobile device or OS based special toolkit to test you app on 5-10
                real
                devices by the price of one</h5>

            <font>
                <h3 class="cufon" align="center">Phones | Tablets | Android developers toolkits | iOS developers
                    toolkits</h3>

                <h3 class="cufon">Phones rent</h3>
            </font>

        </div>

        <div class="col-md-12 col-sm-12">
            <h4>Samsung Galaxy Y</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/samsung_galaxy_y.jpg" alt="Rent Samsung Galaxy Y for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General: </strong>Perfectly suites for testing Android applications on low performance devices
                with small screen <br>
                <strong>OS: </strong>Android 2.3.5 (Gingerbread) (upgradeable to 2.3.6)<br>
                <strong>Screen: </strong>240×320 pixels, 3.0 inch (133 ppi pixel density) TFT capacitive touchscreen,
                262144 colors, 18-bit, 60Hz Refresh Rate<br>
                <strong>CPU: </strong>Broadcom BCM21553 ARM11 832 MHz processor, ARMv6<br>
                <strong>Removable storage</strong> available<br>
                <strong>Network and connectivity: </strong>GSM 850/900/1800/1900, HSDPA 7.2 Mbps 900/2100, WiFi (802.11
                b/g/n) Bluetooth 3.0 <br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table" style="width: 100%">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>0.99€ (0.99€)</td>
                    </tr>
                    <tr>
                        <td>3 days</td>
                        <td>2.59€ (0.86€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>4.99€ (0,71€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>7.99 (0,57€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>14.99 (0,49€)</td>
                    </tr>
                </table>
            </div>

        </div>




        <div class="col-md-12 col-sm-12">
            <h4>Samsung Galaxy Ace</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/samsung_galaxy_ace_2.jpg" alt="Rent Samsung Galaxy Ace for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> Phone for testing applictions on mini / medium devices
                <br>
                <strong>OS:</strong> Android v2.3.6 (Gingerbread) unofficially upgradable to 4.4.2 KitKat via
                CyanogenMod 11 <br>
                <strong>Screen: </strong> TFT LCD, 320×480 pixels HVGA (165 ppi) 16M colors<br>
                <strong>CPU: </strong>800 MHz Qualcomm MSM7227-1 Turbo, ARMv6 <br>
                <strong>Removable storage</strong> available<br>
                <strong>Network and connectivity: </strong> 3G 850/900/1800/1900 MHz; 3.5G HSDPA 7.2 Mbps 900/2100 MHz,
                Wi-Fi 802.11 b/g/n<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>1.69€ (1.69€)</td>
                    </tr>
                    <tr>
                        <td>3 days</td>
                        <td>4.49€ (1.49€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>7.99€ (1,14€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>14.99€ (1,07€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>26.99€ (0,89€)</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <h4>Samsung Galaxy Note 3</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/Samsung_Galaxy_Note_3.jpg" alt="Rent Samsung Galaxy Note 3 for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> Phone for testing powerful applications on premium-class
                Android devices <br>
                <strong>OS:</strong> Android 4.3 "Jelly Bean"<br>
                <strong>Screen: </strong> 5.7 in (145 mm) Full HD Super AMOLED 388 ppi (1920×1080) (PenTile matrix)
                (16:9 aspect ratio) <br>
                <strong>CPU: </strong> 8-core 1.9 GHz Cortex-A15 and 1.3 GHz Cortex-A7 <br>
                <strong>Removable storage</strong> available<br>
                <strong>Network and connectivity: </strong> 3G 850/900/1800/1900 MHz; 3.5G HSDPA 7.2 Mbps 900/2100 MHz,
                Wi-Fi 802.11 b/g/n<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>5.99€ (5.99€)</td>
                    </tr>
                    <tr>
                        <td>3 days</td>
                        <td>16.49€ (5.50€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>29.99€ (4.28€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>54.99€ (3.93€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>99.99€ (3.33€)</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <h4>Samsung Galaxy S3</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/Samsung_Galaxy_S3.jpg" alt="Samsung Galaxy S3 for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> One of the most popular Android Phones in the world that
                could be taken an ethalon for Android phone application testing<br>
                <strong>OS:</strong> Android 4.0.4/4.1.2 "Ice Cream Sandwich" / "Jelly Bean" (TouchWiz "Nature UX"
                GUI)<br>
                <strong>Screen: </strong> 4.8 in (120 mm) HD Super AMOLED (720×1280)<br>
                <strong>CPU: </strong> 1.4 GHz quad-core Cortex-A9<br>
                <strong>Removable storage</strong> available<br>
                <strong>Network and connectivity: </strong> 2G GSM/GPRS/EDGE – 850, 900, 1800, 1900 MHz, 3G
                UMTS/HSPA+/CDMA2000 – 850, 900, 1700, 1900, 2100 MHz, 4G LTE – 700, 800, 1700, 1800, 1900, 2600 MHz,
                Wi-Fi (802.11 a/b/g/n)
                Wi-Fi Direct<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>3.49€ (3.49€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>9.99€ (3.33€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>17.99€ (2.57€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>32.99€ (2.36€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>59.99€ (2.00€)</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <h4>Samsung Galaxy S4 Mini</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/samsung_galaxy_s4_mini.jpg" alt="Samsung Galaxy S4 mini for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> One of the most popular Android Phones in the world that
                could be taken an ethalon for Android phone application testing <br>
                <strong>OS:</strong> Android 4.2.2 "Jelly Bean" <br>
                <strong>Screen: </strong> 4.3 in (110 mm) RGB 540x960 px qHD (256 PPI)<br>
                <strong>CPU: </strong> 1.7 GHz dual-core Krait 300<br>
                <strong>Removable storage</strong> Up to 64 GB microSDXC<br>
                <strong>Network and connectivity: </strong> 2G GSM/GPRS/EDGE – 850, 900, 1800, 1900 MHz, 3G UMTS/HSPA+ –
                850, 900, 1900, 2100 MHz, Wi-Fi (802.11a/b/g/n); Wi-Fi Direct; Bluetooth 4.0; <br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>3.99€ (3.99€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>10.99€ (3.66€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>19.99€ (2.86€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>36.99€ (2.64€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>66.99€ (2.23€)</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <h4>IPhone 4</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/iPhone4.jpg" alt="IPhone 4 for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> Ethalon for testing iPhone applications<br>
                <strong>OS:</strong> Preinstalled by request (from iOS 4.0 to iOS 7.1.1<br>
                <strong>Screen: </strong> 3.5 in widescreen 960×640 resolution at 326 ppi<br>
                <strong>CPU: </strong> ARM Cortex-A8<br>
                <strong>Removable storage</strong> Not available<br>
                <strong>Network and connectivity: </strong> Bluetooth 2.1 + EDR, GSM model: quad-band GSM/GPRS/EDGE,
                (800, 850, 900, 1,800, 1,900 MHz), Quad-band UMTS/HSDPA/HSUPA, (800, 850, 900, 1,900, 2,100 MHz), Wi-Fi
                (802.11 b/g/n) <br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>3.99€ (3.99€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>10.99€ (3.66€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>19.99€ (2.86€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>36.99€ (2.64€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>66.99€ (2.23€)</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <h4>IPhone 5C</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/iphone5c.jpg" alt="IPhone 5C for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> Ethalon for testing latest iPhone applications <br>
                <strong>OS:</strong> Preinstalled by request (from iOS 7.0 to iOS 7.1.1)<br>
                <strong>Screen: </strong> TFT LCD, 640×1136 pixels (326 ppi)<br>
                <strong>CPU: </strong> 1.3 GHz dual-core<br>
                <strong>Removable storage</strong> not available<br>
                <strong>Network and connectivity: </strong> GSM, 3G, EVDO, HSPA+, LTE, Wi-Fi (802.11 b/g/n) <br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>5.99€ (5.99€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>18.49€ (6.16€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>33.99€ (4.86€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>60.99€ (4.36€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>109.99€ (3.67€)</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <h4>NOKIA Lumia 620</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/nokia_lumia_620.jpg" alt="NOKIA Lumia 620 for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> Phone for testing Windows Phone applicatioтs<br>
                <strong>OS:</strong> Windows Phone 8<br>
                <strong>Screen: </strong> 10 point Multi-touch capacitive touchscreen, 800×480 px <br>
                <strong>CPU: </strong> 1.0 GHz dual-core Qualcomm Krait MSM8227<br>
                <strong>Removable storage</strong> Hot Swappable, MicroSD (up to 64 GB)<br>
                <strong>Network and connectivity: </strong> GSM/GPRS/EDGE 850/900/1800/1900, HSPA 850/900/1900/2100,
                Wi-Fi,<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>2.49€ (2.49€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>5.99€ (2.00€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>10.99€ (1.57€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>18.99€ (1.36€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>33.99€ (1.13€)</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <h4>NOKIA Lumia 1320</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/nokia_lumia_1320.jpg" alt="NOKIA Lumia 1320 for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> Testing on LUMIA 1320 is the perfect choice if you are
                looking the ways to improve your mobile app<br>
                <strong>OS:</strong> Windows Phone 8 <br>
                <strong>Screen: </strong> 6" multi-touch capacitive touchscreen, 1280x720 px<br>
                <strong>CPU: </strong> 1.7 GHz dual-core Qualcomm<br>
                <strong>Removable storage</strong> MicroSD (up to 64 GB)<br>
                <strong>Network and connectivity: </strong> GSM/GPRS/EDGE, HSPA+, 4G LTE Rel. 8 (UE Cat 3), Wi-Fi<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>3.99€ (3.99€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>10.99€ (3.66€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>19.99€ (2.86€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>36.99€ (2.64€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>66.99€ (2.23€)</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <h3 class="cufon">Tablets rent</h3>
            <h4>Samsung Galaxy Tab 2</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/samsung_galaxy_tab_2.jpg" alt="Samsung Galaxy Tab 2 for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> Ethalon to test Android apps for tablets <br>
                <strong>OS:</strong> Android 4.0 Ice Cream Sandwich (upgradable to 4.1.2 Jelly Bean and to 4.2.2 Jelly
                Bean) <br>
                <strong>Screen: </strong> 1280x800 px, 10.1", Multi-touch screen <br>
                <strong>CPU: </strong> 1.0 GHz dual-core TI OMAP4430 (Cortex A9) SoC processor<br>
                <strong>Removable storage</strong> microSDXC slot (up to 32 GB)<br>
                <strong>Network and connectivity: </strong> EDGE/GPRS 850/900/1800/1900 MHz (3G & WiFi model), Wi-Fi
                802.11a/b/g/n, Bluetooth 3.0, HDMI by external cable<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>3.49€ (3.49€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>9.99€ (3.33€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>17.99€ (2.57€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>32.99€ (2.36€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>59.99€ (2.00€)</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <h4>Samsung Galaxy NOTE 10.1</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/samsung_galaxy_note_10_1.png" alt="Samsung Galaxy Note 10.1 for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> Ethalon for testing Android apps for tablets with stylus
                <br>
                <strong>OS:</strong> Android 4.0.4 Ice Cream Sandwich with TouchWiz UI, upgradeable to Android 4.4.2
                KitKat<br>
                <strong>Screen: </strong> 1280×800, 10.1"<br>
                <strong>CPU: </strong> 1.4 GHz quad-core ARM Cortex-A9<br>
                <strong>Removable storage</strong> up to 64 GB with microSD card<br>
                <strong>Network and connectivity: </strong> 3G, WiFi<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>4.99€ (4.99€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>13.99 (4.66€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>24.99€ (3.57€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>45.99€ (3.29€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>83.99€ (2.80€)</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <h4>ASUS Google Nexus 7</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/google_nexus_7.jpg" alt="Google Nexus 7 for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> The Galaxy Nexus is one of the few smartphones recommended
                by the Android Open Source Project for Android software development<br>
                <strong>OS:</strong> Android 4.3 Jelly Bean (Upgradable to 4.4.2 KitKat) <br>
                <strong>Screen: </strong> 7.02", 1920×1200, (323 ppi)<br>
                <strong>CPU: </strong> 1.51 GHz quad-core Krait 300<br>
                <strong>Removable storage</strong> available<br>
                <strong>Network and connectivity: </strong> WiFi only<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>3.99€ (3.99€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>10.99€ (3.66€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>19.99€ (2.86€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>36.99€ (2.64€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>66.99€ (2.23€)</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <h4>iPAD I Generation</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/apple-ipad-first-generation.jpg" alt="IPad-1"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>General:</strong> Perfect choice to test iPad app performance <br>
                <strong>OS:</strong> iOS 5.1.1 <br>
                <strong>Screen: </strong> 1024×768 pixels at 132 ppi<br>
                <strong>CPU: </strong> 1 GHz ARM Cortex-A8<br>
                <strong>Removable storage</strong> not available<br>
                <strong>Network and connectivity: </strong> 3G, Wi-Fi 802.11 a/b/g/n<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>2.99€ (2.99€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>7.99€ (2.66€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>14.99€ (2.14€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>27.99€ (2.00€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>49.99€ (1.67€)</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <h4>iPAD 2 Mini</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/Ipad2_mini.jpg" alt="Ipad-2-mini for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>OS:</strong> Preinstalled by request (iOS 6.0 up to 7.1.1) <br>
                <strong>Screen: </strong> 7.9" 1024 × 768 px at 163 PPI<br>
                <strong>CPU: </strong> 1 GHz Apple dual-core A5<br>
                <strong>Removable storage</strong> not available<br>
                <strong>Network and connectivity: </strong> 3G, WiFi<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>3.99€ (3.99€)</td>
                    </tr>

                    <tr>
                        <td>3 days</td>
                        <td>10.99€ (3.66€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>19.99€ (2.86€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>36.99€ (2.64€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>66.99€ (2.23€)</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">

            <h4>iPAD 3</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/apple_ipad_3_white.jpg" alt="IPad-3"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>OS:</strong> Preinstalled by request (from iOS 5.1 to iOS 7.1.1) <br>
                <strong>Screen: </strong> 9.7" 2048 × 1536 px (264 ppi)<br>
                <strong>CPU: </strong> 1 GHz[2] dual-core ARM Cortex-A9<br>
                <strong>Removable storage</strong> not available<br>
                <strong>Network and connectivity: </strong> Wi-Fi (802.11a/b/g/n), Bluetooth 4.0<br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>5.99€ (5.99€)</td>
                    </tr>
                    <tr>
                        <td>3 days</td>
                        <td>16.49€ (5.50€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>29.99€ (4.28€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>54.99€ (3.93€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>99.99€ (3.33€)</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <h3 class="cufon">Android developers toolkits</h3>
            <h4>Android developer professional toolkit</h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/android-logo-png.png" alt="tablets"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>About toolkit: includes Samsung Galaxy Tab 2 + Samsung Galaxy Note 10.1 + Samsung Galaxy Y +
                    Galaxy Note 3 + Galaxy S3 + Galaxy S4 Mini + Asus Google Nexus 7</strong><br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>23.99€ (23.99€)</td>
                    </tr>
                    <tr>
                        <td>3 days</td>
                        <td>63.99€ (21,33€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>116.99€ (16.71€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>212.99€ (15.21€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>386.99€ (12.90€)</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <h3 class="cufon">iOS developers toolkits</h3>
            <h4>iOS professional toolkit </h4>

            <div class="col-md-4 col-sm-12">
                <img src="/tmp/ios.png" alt="iOS professional toolkit for testing"/>
            </div>
            <div class="col-md-4 col-sm-12">
                <strong>Professional toolkit includes IPad I + Ipad II + Ipad III + Iphone 4 + Iphone 5c</strong><br>
            </div>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td><strong>Period</strong></td>
                        <td><strong>Rent price (per day)</strong></td>
                    </tr>
                    <tr>
                        <td>1 day</td>
                        <td>23.99€ (23.99€)</td>
                    </tr>
                    <tr>
                        <td>3 days</td>
                        <td>64.99€ (21.66€)</td>
                    </tr>
                    <tr>
                        <td>7 days</td>
                        <td>118.99€ (17.00€)</td>
                    </tr>
                    <tr>
                        <td>2 weeks</td>
                        <td>215.99€ (15.43€)</td>
                    </tr>
                    <tr>
                        <td>1 month</td>
                        <td>392.99€ (13.10€)</td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>

<?php
include($_SERVER['DOCUMENT_ROOT'] . "/footer.php");
?>

</div> <!-- /main -->

</div> <!-- /bg -->

<!--<script type="text/javascript"> Cufon.now(); </script>-->

</body>
</html>
