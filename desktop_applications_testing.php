﻿<?php
$pageKeywords = 'Desktop, software, testing, applications';
$pageTitle = 'γ-Test: Desktop applications testing';
$pageDescription = 'γ-Test: Professional desktop applications testing';
include($_SERVER['DOCUMENT_ROOT'] . "/header.php");
?>
<!-- CONTENT -->
<div class="row pagecontent">
    <div class="content box col-md-12">
        <br>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="We test it"/>Functional testing</h2>

            <p class="thumb"><img src="tmp/desktop_application_functional_testing.jpg"
                                  alt="Desktop applications functional testing"/></p>
            <h5>Are the functional requirements fully implemented? Could the functionality be optimized and number of
                screen-flows reduced? <a href="/contact.php">Get in touch with <strong>γ-Test</strong></a> and we will
                take functionality on the next level:</h5>
            </p>
        </div>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="We test it"/>Compatibility testing</h2>

            <p class="thumb"><img src="tmp/desktop_application_compatibility_testing.jpg"
                                  alt="Desktop applications compatibility testing"/></a></p>
            <h5>Are you sure that your app is running on old devices as well as on the newest? Are you ready to lost old
                devices auditory for your app? <a href="/contact.php">Ask us</a> and we help you to reach the maximum
                compatibilty of your application, providing a full testing in next key areas:</h5>
            <!--img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Testing on old / low performance devices</b></br>
          <img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Interoperability testing</b></br>
            <img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Installability testing</b></br>
            <img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Performance testing</b></br-->
        </div>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="We test it"/>Performance testing</h2>

            <p class="thumb"><img src="tmp/desktop_application_performance_testing.png"
                                  alt="Desktop applications performance testing"/></a></p>

            <h5>How your application is working on older devices? What is the energy and memory consumption? Is your
                application having a memory leaks? <a href="/contact.php">Ask us</a> and we will provide you a full
                picture of your app performance including the next:</h5>

            <!--img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Mobile application testying on devices with different CPU's / different RAM amount</b></br>
            <img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Graphic performance testing for Game / 3D mobile applications</b></br>
            <img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Network connecivity testing (server response time and packet loss) </b></br-->
        </div>

        <div class="clearfix"></div>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="We test it"/>Business apps test</h2>

            <p class="thumb"><img style="display: inline:1nline-block;width:100%"
                                  src="tmp/desktop_business_applications_testing.jpg"
                                  alt="Desktop applications testing for business"/></a></p>

            <h5>Are the application fully satisfies customer business requirements? Are the personal using built
                software in right way? <a href='/contact.php'><a href="/contact.php">Ask us</a> for a testing plan</a>
                and we will validate your software due to business processes.</h5>

        </div>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="We test it"/>PC games testing</h2>

            <p class="thumb"><img src="tmp/desktop_PC_games_testing.gif" alt="PC desktop dames testing"/></a></p>

            <h5>Are you sure that your game is interesting for player? Are the player enough involved in your game from
                first seconds? Are you game enough balanced to be honest for all kinds of player? <a
                    href="/contact.php">Contact us</a> and we make your game more exciting</p></h5>

        </div>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="We test it"/>Localization testing</h2>

            <p class="thumb"><img src="tmp/Desktop_localisation_and_internationalisation.jpg"
                                  alt="Desktop applications localization, translation and internationalization"/></a>
            </p>

            <h5>Are your app easily understandable in the market it's targeting? Are your taking into account such
                factors like formatting and cultural considerations? Whether you think about translation, localization
                or internationalization <a href='/contact.php'>ask our localization team</a> to help you conquer all
                possible markets</h5>

        </div>

    </div>
</div>

<?php
include($_SERVER['DOCUMENT_ROOT'] . "/footer.php");
?>

</div> <!-- /main -->

</div> <!-- /bg -->

<!--<script type="text/javascript"> Cufon.now(); </script>-->

</body>
</html>
