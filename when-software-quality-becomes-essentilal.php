﻿
<?php 
	include ($_SERVER['DOCUMENT_ROOT']."/header.php");
?>


<!-- CONTENT -->
		<div class="content box">
		   
			<!-- HEADINGS -->
			<h2 class="cufon"><strong><i>When the software quality becomes essential?</i></strong></h2>
			<p> 
			When I studied in university, I heard many times from my mentor remembered words: <strong>"Software that does not work as expected is the same as it does not exist."</strong> So, when you developed a software you had first to write a requirements and your program should work exactly as it was expected from the beginning.
			</p>
			<p>
			However, years of working in the software development industry, demonstrated that a little bit different approach is actual: <strong>"Software should do what we are expecting and should not do what we are not expecting"</strong>. In turn, this could be achieved only in case of developing some very simple solutions. Taking into account complexity of contemporary software it could not be achieved. Here came the next approach: <strong>"Profit of implementing software with certain level of quality should exceed the cost of realizing most bad scenario multiplied by probability of that scenario appearance"</strong>.  In other words, quality of the software in most cases should met the some certain levels, sometimes exceeding them, but also common sense should be taken into account.
			</p>
			<p>
			Basically, this approach to software quality is a risk-based approach. So, quality of software, becomes an essential when a certain risk presents that lack of quality could proceed the losses. Here some samples.
			</p>
			<h3 class="cufon"><strong><i>Real-time systems software</i></strong></h3>
			<p class="box">
			Imagine that you are building highly automated power plant that requires minor participation from human side. All equipment (including, e.g. boilers with extremely high pressure), processes and emergency systems are managed by software. In such case, unexpected behavior caused by the bug in the system could lead not only loss of money, but also loss of human lifes
			</p>
			<h3 class="cufon"><strong><i>One-time released software</i></strong></h3>
			<p class="box">
			Imagine that you are owner of the software development company producing the computer games. Thousands of man-hours had been spent to built unrepeatable game universe with unique game-play. However, there is one сircumstance: your game is released on the СD or DVD, which do not allow to deploy hot-fix in case if something is broken. What you will do, if after years of waiting, your customers will receive a game that crashes in the most exciting moments?
			</p>
			<h3 class="cufon"><strong><i>Software based on integrated components</i></strong></h3>
			<p class="box">
			Very often it is needed to combine and integrate different applications and services into one system in order to achieve some benefits (good sample here is a corporate software or some web-page created using different tchologies). Despite the fact, that each part could be reliable and free of bugs, integration of separate components could cause an unexpected behave, because "communication" between different applications оссurs without human involvement. With high probability, each component of the system will receive a new inputs, will generate new outputs and this could cause a totally unpredictable behaviour paralysing whole system.
			</p>
			<h3 class="cufon"><strong><i>Error cost is too high</i></strong></h3>
			<p class="box">
			In common, it is an extension of the case given about real-time systems. But, from this point of view, error cost means not only the money (or, in worst case, human lives), but also includes such kind of losses as reputation, trust, customer loyalty or competetitive advantage. Additionally, it may cause a loss of certification due to legislation non-compliances.
			</p>
			<h3 class="cufon"><strong><i>As a conclusion</i></strong></h3>
			<p class="box">
			Of course, given above samples list could be extended. Nevertheless, aim of this topic is not coverage of all possible situations, but demonstration that approach to the software quality is the same as providing quality for everything. Generally, software has a value only if it brings some value in human life. If this value is threatened by bad software quality the last one become essential and certailnly should be improved.
			</p>
	

			</div> 		

	<?php 
	include ($_SERVER['DOCUMENT_ROOT']."/footer.php");
?>

</div> <!-- /main -->

</div> <!-- /bg -->

<!--<script type="text/javascript"> Cufon.now(); </script>-->

</body>
</html>
