﻿<?php 
	$pageKeywords='software engineering, mobile applications testing, software testing, contact, Estonia, Tallinn';
	$pageTitle = 'γ-Test: Contact us';
	$pageDescription = 'Ask us about software testing';

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'].'/gamma-test/';

include ($_SERVER['DOCUMENT_ROOT']."/header.php");
?>
		
		<!-- CONTENT -->

<div class="row pagecontent">
    <div class="content box col-md-12">

        <div class="row">
            <div class="col-md-6">

                <form action="mail.php" method="post" role="form">
                    <h2 class="cufon" align="left">Ask us about testing</h2>

                    <div class="form-group">
                        <label for="Subject">Subject <font color="red">*</font>:</label>
                        <select type="text" cols="27" size="1" class="input-text form-control" name="type">
                            <option value="Software Engineering">Software engineering support</option>
                            <option value="Desktop applications">Desktop applications testing</option>
                            <option value="Mobile applications">Mobile applications testing</option>
                            <option value="Web testing">Web testing</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="Email">E-mail: <font color="red">*</font>:</label>
                        <input type="text" size="25" class="input-text form-control" id="input-03" name="email">
                    </div>

                    <div class="form-group">
                        <label for="Name">Your name: <font color="red">*</font>:</label>
                        <input type="text" size="25" class="input-text form-control" id="name" name="name">
                    </div>

                    <div class="form-group">
                        <label for="Company">Your company:</label>
                        <input type="text" size="25" class="input-text form-control" id="company" name="company">
                    </div>

                    <div class="form-group">
                        <label for="Phone">Your phone: <font color="red">*</font>:</label>
                        <input type="text" size="25" class="input-text form-control" id="phone" name="phone">
                    </div>

                    <div class="form-group">
                        <label for="Skype">Skype:</label>
                        <input type="text" size="25" class="input-text form-control" id="skype" name="skype">
                    </div>

                    <div class="form-group">
                        <label for="Message">Request or message:</label>
                        <textarea cols="100" rows="5" class="input-text form-control" id="input-06"
                                  name="message"></textarea>
                    </div>

                    <div class="field form-inline radio col-md-12 col-sm-12">
                        <input class="radio" type="radio" name="feedback2" value="subscribed" checked/> <span>I would like to receive newsletters and special offers from Gammatest (not often than 1 time per month)</span><br>
                        <input class="radio" type="radio" name="feedback2" value="not-subscribed"/> <span>Do not want to recevie a newsletters and special offers</span>
                    </div>

                    <button type="submit" class="btn btn-default input-submit">Send</button>

                </form>

            </div>

            <div class="col-md-6">
                <h2 class="cufon pull-left" align="center">Our contacts</h2>

                <div class="clearfix"></div>
                <div class="pull-left">
                    <strong>GAMMA TEST TECHNOLOGIES OÜ</strong><br>
                    <strong>Aadress:</strong> 13920, Priisle tee 8-80, Tallinn, Estonia<br>
                    <strong>Phone:</strong> +372 56 319 339<br>
                    <strong>Email:</strong> <a href="mailto:info@gammatest.net">info@gammatest.net</a><br>
                </div>
            </div>
        </div>

    </div>
</div>


	<?php 
	include ($_SERVER['DOCUMENT_ROOT']."/footer.php");
?>

</div> <!-- /main -->

</div> <!-- /bg -->

<!--<script type="text/javascript"> Cufon.now(); </script>-->

</body>
</html>
