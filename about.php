﻿<?php
$pageKeywords = 'outsource, software, testing, applications, qa, quality assurance';
$pageTitle = 'γ-Test: About γ-Test';
$pageDescription = 'γ-Test is a software testing company headquartered in Tallinn, Estonia. We support and improve software development processes, providing consultancy in requirements analysis, QA and outsource testing.';

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'].'/gamma-test/';

include($_SERVER['DOCUMENT_ROOT'] . "/header.php");
?>

<!-- CONTENT -->
<div class="row pagecontent">
<div class="content box col-md-12">
<br>
<!-- HEADINGS -->
<div class="col-md-12 col-sm-12">
    <h3 class="cufon"><strong><i>About Gamma test</i></strong></h3>

    <p>
        Gamma test is a company head-quartered in Tallinn, Estonia. We support and improve software
        development processes, providing consultancy in requirements analysis, QA and outsource testing.
    </p>
</div>

<div class="col-md-4 col-sm-12">
    <h3 class="cufon"><strong><i>Mobile applications testing</i></strong></h3>

    <p class="box"><img src="tmp/mobile_application_testing.png" alt="Mobile applications testing" class="f-left"/>
        Complete testing for mobile applications is our strongest area. Our QA mobile engineers had tested more than
        50 mobile applications on different platforms and devices with varying level of complexity. We had tested
        simple Android 5-pages apps as well as e.g. complex geo-location mobile services.
    </p>
</div>

<div class="col-md-4 col-sm-12">
    <h3 class="cufon"><strong><i>Desktop applications testing</i></strong></h3>

    <p class="box"><img src="tmp/desktop_applications_testing.jpg" alt="Desktop applications testing"
                        class="f-left"/>
        Databases, CAD/CAMs, Games - it is not complete list of projects where we tested desktop applications. We
        are concentrating on clear functionality as well as on the perfomance and installability.
    </p>
</div>

<div class="col-md-4 col-sm-12">
    <h3 class="cufon"><strong><i>Web testing</i></strong></h3>

    <p class="box"><img src="tmp/web_testing.png" alt="Web testing" class="f-left"/>
        Working closely with business requirements we had tested more than 100 different webpages and web services.
        We had tested web-solutions for all areas they could be used, including gambling, geo-locational and
        financial services. In most projects we had taken into account not only business requirements, but also
        e-commerce component, search engine optimization, third party integrated services and many many more...
    </p>
</div>

<div class="col-md-12 col-sm-12">
    <h3 class="cufon"><strong><i>Embedded applications and real-time systems testing</i></strong></h3>

    <p class="box"><img src="tmp/embedded_software_testing.jpg"
                        alt="Embedded applications and real-time systems testing" class="f-left"/>
        Successful participation in comissioning and launching complicated real-time systems make us proud. Our
        QA-engineers provided Factory Acceptance Testing (FAT) as well as Site Acceptance Testing (SAT) for such
        real-time systems as power plants, marine automation systems, industrial conveyor systems and industrial
        robots. Also we could always perform software quality control for industrial controllers, embedded systems
        and programmable logic controllers.
    </p>
</div>

<div class="col-md-12 col-sm-12">
    <p>
        Any time our experienced team is ready to help you improve the quality of your software. Does not matter,
        would you like to test mobile, desktop, web ot even embedded application. Contact us and we will help you.
    </p>

    <div class="row">
        <div class="col-md-6">

            <form action="mail.php" method="post" role="form">
                <h2 class="cufon" align="left">Ask us about testing</h2>

                <div class="form-group">
                    <label for="Subject">Subject <font color="red">*</font>:</label>
                    <select type="text" cols="27" size="1" class="input-text form-control" name="type">
                        <option value="Software Engineering">Software engineering support</option>
                        <option value="Desktop applications">Desktop applications testing</option>
                        <option value="Mobile applications">Mobile applications testing</option>
                        <option value="Web testing">Web testing</option>
                        <option value="Other">Other</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="Email">E-mail: <font color="red">*</font>:</label>
                    <input type="text" size="25" class="input-text form-control" id="input-03" name="email">
                </div>

                <div class="form-group">
                    <label for="Name">Your name: <font color="red">*</font>:</label>
                    <input type="text" size="25" class="input-text form-control" id="name" name="name">
                </div>

                <div class="form-group">
                    <label for="Company">Your company:</label>
                    <input type="text" size="25" class="input-text form-control" id="company" name="company">
                </div>

                <div class="form-group">
                    <label for="Phone">Your phone: <font color="red">*</font>:</label>
                    <input type="text" size="25" class="input-text form-control" id="phone" name="phone">
                </div>

                <div class="form-group">
                    <label for="Skype">Skype:</label>
                    <input type="text" size="25" class="input-text form-control" id="skype" name="skype">
                </div>

                <div class="form-group">
                    <label for="Message">Request or message:</label>
                    <textarea cols="100" rows="5" class="input-text form-control" id="input-06"
                              name="message"></textarea>
                </div>

                <div class="field form-inline radio col-md-12 col-sm-12">
                    <input class="radio" type="radio" name="feedback2" value="subscribed" checked/> <span>I would like to receive newsletters and special offers from Gammatest (not often than 1 time per month)</span><br>
                    <input class="radio" type="radio" name="feedback2" value="not-subscribed"/> <span>Do not want to receive a newsletters and special offers</span>
                </div>

                <button type="submit" class="btn btn-default input-submit">Send</button>

            </form>

        </div>

        <div class="col-md-6">
            <h2 class="cufon pull-left" align="center">Our contacts</h2>

            <div class="clearfix"></div>
            <div class="pull-left">
                <strong>GAMMA TEST TECHNOLOGIES OÜ</strong><br>
                <strong>Aadress:</strong> 13920, Priisle tee 8-80, Tallinn, Estonia<br>
                <strong>Phone:</strong> +372 56 319 339<br>
                <strong>Email:</strong> <a href="mailto:info@gammatest.net">info@gammatest.net</a><br>
            </div>
        </div>
    </div>

</div>

</div>
</div>

<?php
include($_SERVER['DOCUMENT_ROOT'] . "/footer.php");
?>

</div> <!-- /main -->

</div> <!-- /bg -->

<!--<script type="text/javascript"> Cufon.now(); </script>-->

</body>
</html>
