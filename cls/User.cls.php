<?php 

class User extends Item{
	
	var $tablename;
	
	var $id;
	var $role_id;
	var $name;
	var $email;
	var $username;
	var $password;
    var $phone;
    var $company;
    var $skype;
    var $created_date;
    var $updated_date;
    var $visible;
    var $deleted;

	var $error;
	
	function __construct() {
		$this->tablename = 'users';
		return $this;
	}

	function loadUser($id=''){
		
		if($id != '') $sql = "SELECT * FROM `'.$this->tablename.'` WHERE deleted = 0 AND id = '".$id."' ";
		else $sql = "SELECT * FROM '.$this->tablename.' WHERE visible = 1 AND deleted = 0 ";
		if($this->Filter != '')	$sql .= ' AND '.$this->Filter;

		return $this->select($sql);

	}
	
	function saveUser($fields){
		
		if(isset($fields['id'])){

			$sql = 'UPDATE `'.$this->tablename.'` SET `name` = "'.$fields['name'].'", `email` = "'.$fields['email'].'", `username` = "'.$fields['username'].'", ';
			$sql .= '`password` = "'.$this->MakeSafe($this->encrypt($fields['password'])).'", `phone` = "'.$fields['phone'].'", `company` = "'.$fields['company'].'", `skype` = "'.$fields['skype'].'",  ';
			$sql .= '`updated_date` = NOW(), visible = "'.$fields['visible'].'" WHERE id = "'.$fields['id'].'" ';

			if($this->Filter != '')	$sql .= ' AND '.$this->Filter;
		}
		else{		
			
			$sql = 'INSERT INTO `'.$this->tablename.'` SET `name` = "'.$fields['name'].'", `email` = "'.$fields['email'].'", `username` = "'.$fields['username'].'", ';
            $sql .= '`password` = "'.$this->MakeSafe($this->encrypt($fields['password'])).'", `phone` = "'.$fields['phone'].'", `company` = "'.$fields['company'].'", `skype` = "'.$fields['skype'].'",  ';
            $sql .= '`created_date` = NOW() ';

		}

		mysql_query($sql) or die('Error save content: '.mysql_error().$sql);
		
		if($fields['id'] != '0') $id = $fields['id'];
		else $id = mysql_insert_id();
		
		return $id;
		
	}
		
	
	function userLogin($fields){

        $sql = 'SELECT * FROM `'.$this->tablename.'` WHERE `username` = "'.$this->MakeSafe($fields['username']).'" AND `password` = "'.$this->MakeSafe($this->encrypt($fields['password'])).'" AND `visible` = 1 AND `deleted` = 0 ';
		if($this->Filter != '')	$sql .= ' AND '.$this->Filter;

		$result = $this->select($sql);

		if(!empty($result)){
			for($i=0; $i<count($result); $i++){
				$this->username = $result[$i]['username'];
				$this->password = $result[$i]['password'];
				$this->visible = $result[$i]['visible'];

				$_SESSION['SYSTEM']['USER']['username'] = $result[$i]['username'];
				$_SESSION['SYSTEM']['USER']['id'] = $result[$i]['id'];
				
			}

			echo '<script>window.location = "index.php";</script>';

		}else{
			$this->error = 'Invalid user login!';
			return false;
		}
	}
	
	function getUserPasswordByEmail($email){
		
		$sql = "SELECT * FROM `'.$this->tablename.'` WHERE visible = 1 AND deleted = 0 AND email = '".$this->MakeSafe($email)."' ";
		if($this->Filter != '')	$sql .= ' AND '.$this->Filter;
		
		$content = $this->select($sql);
		
		if(!empty($content)){
			for($i=0; $i<count($content); $i++){
				$this->username = $content[$i]['username'];
				$this->password = $content[$i]['password'];
				$this->email = $content[$i]['email'];
				$this->name = $content[$i]['name'];
				
				$this->visible = $content[$i]['visible'];
				$this->deleted = $content[$i]['deleted'];
			}
		}
		
		return $this;

	}
	
	public static function checkUserLogin(){

		if(!isset($_SESSION['SYSTEM']['USER']['username'])){	echo '<script>window.location = "user_login.php";</script>';}
		else return true;

	}

    public static function getUserName(){

        if(!isset($_SESSION['SYSTEM']['USER']['username'])){
            return '';
        }
        else return $_SESSION['SYSTEM']['USER']['username'];

    }

    public static function logOutUser(){

        session_destroy();

        return true;
    }
	
	public static function setUserSession($data){
		
		$_SESSION['SYSTEM']['USER']['id'] = $data['id'];
		$_SESSION['SYSTEM']['USER']['username'] = $data['username'];
		$_SESSION['SYSTEM']['USER']['visible'] = $data['visible'];
		$_SESSION['SYSTEM']['USER']['name'] = $data['name'];
		echo '<script>$(document).ready(function(){$(".success").fadeIn();});</script>';	

	}

	function removeUser($fields){

		if($fields['deleted'] == 1) $fields['deleted'] = 0;
		else if($fields['deleted'] == 0) $fields['deleted'] = 1;
	
		$sql = "UPDATE `'.$this->tablename.'` SET `deleted` = '".$fields['deleted']."' WHERE `id` = '".$fields['id']."' ";
		if($this->Filter != '')	$sql .= ' AND '.$this->Filter;
		mysql_query($sql) or die('Error delete user : '.mysql_error().$sql);

	}

	function visibleUser($fields){
		
		if($fields['visible'] == 1) $fields['visible'] = 0;
		else if($fields['visible'] == 0) $fields['visible'] = 1;

		$sql = "UPDATE `'.$this->tablename.'` SET `visible` = '".$fields['visible']."' WHERE `id` = '".$fields['id']."' ";
		if($this->Filter != '')	$sql .= ' AND '.$this->Filter;
		mysql_query($sql) or die('Error delete user : '.mysql_error().$sql);

	}
	
	function isRegisteredUserEmail($email){
		
		$sql = 'SELECT email FROM `'.$this->tablename.'` WHERE email = "'.$email.'" AND visible = 1 AND deleted = 0 ';
		if($this->isSelect($sql)){
			return 1;	
		}else{
			return 0;
		}
		
	}

    function isRegisteredUsername($username){

        $sql = 'SELECT email FROM `'.$this->tablename.'` WHERE username = "'.$username.'" AND visible = 1 AND deleted = 0 ';
        if($this->isSelect($sql)){
            return 1;
        }else{
            return 0;
        }

    }
	
}


?>