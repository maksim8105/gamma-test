<?php 

class Emails{
	
	function SendEmail($to,$subject,$message,$ishtml=true,$fromEmail='',$fromName='',$toName='') {

		if(!$fromEmail) $fromEmail = EMAIL_ADMIN_FROM;
		if(!$fromName) $fromName = EMAIL_ADMIN_FROMNAME;
		if(!$fromName) $fromName = $fromEmail;
		if(!$toName) $toName = $to;


		try {
			$mail             = new PHPMailer();
			$mail->IsSMTP(); // telling the class to use SMTP

			$mail->Host       = SMTP_SERVER;   // SMTP server
			//$mail->Host       = "relay-hosting.secureserver.net";   // SMTP server
			$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
			                                           // 1 = errors and messages
			                                           // 2 = messages only
			//$mail->MailerDebug =false;
			//$mail->SMTPAuth   = true;                  // enable SMTP authentication
			//$mail->SMTPSecure = "ssl";
			//$mail->Port       = 465;                    // set the SMTP port for the GMAIL server
			//$mail->Username   = "yenuliliyanage@gmail.com"; // SMTP account username
			//$mail->Password   = "yenu1234"; // SMTP account password
			$mail->Username   = SMTP_USERNAME; // SMTP account username
			$mail->Password   = SMTP_PASSWORD; // SMTP account password
				
			//$mail->Priority    = 1; // Highest priority - Email priority (1 = High, 3 = Normal, 5 = low)
			//$mail->Encoding    = '8bit';

			$mail->IsHTML($ishtml);
			$mail->SetFrom($fromEmail, $fromName);
			$mail->AddReplyTo($fromEmail, $fromName);
			$mail->Subject    = $subject;
			$mail->Body    = $message; // optional, comment out and test
			$mail->AddAddress($to, $toName);


			if(!$mail->Send()) {
			  return false;
			} else {
			  return true;
			}
		} catch (phpmailerException  $e) {
			//nothing
		} catch (Exception  $e) {
			//nothing
		}
	}
	
	function SendPasswordForgotEmail($fields) {
		
		//get user password
		$objUser = new User();	
		$user = $objUser->getUserPasswordByEmail($fields['Email']);	

		$toemail = $fields['email'];
		
		$subject = 'Password Reminder from '.COMPANY_NAME;
		$message = "Dear ".$user->Name.", <br/> ";
			$message .= "You have requested for password recovery of your ".COMPANY_NAME." account. Your lost password is <strong>".$user->Password."</strong> <br/><br/>";
			$message .= "Support Team, <br/>";
			$message .= COMPANY_NAME."<br/>";

        return $this->SendEmail($toemail, $subject, $message);

	}

	function sendContactUs($name,$email,$subject,$msg)
	{
		$subject = 'Contact Us Enquiry from '.COMPANY_NAME;
		$message = "Enqury from ".COMPANY_NAME." <br/>";
		  $message .= "Name : ".$name." <br/>";
		  $message .= "Email : ".$email." <br/>";
		  $message .= "Subject : ".$subject." <br/>";
		  $message .= "Message : ".$msg." <br/>";

        return $this->SendEmail(EMAIL_ADMIN_FROM, $subject, $message);
		
	
	}

	function sendRegisterConfirmEmail($data)
	{
		
		$toemail = $data['email'];
		
		$subject = 'Welcome to '.COMPANY_NAME;
		$message = "Dear ".$data['name'].", <br/> ";
			$message .= "You have successfully registered with ".COMPANY_NAME.".<br/>";
			$message .= "Thank you. <br/><br/>";
			$message .= "Support Team, <br/>";
			$message .= COMPANY_NAME."<br/>";	
		 
		return $this->SendEmail($toemail, $subject, $message);
	
	}
	
	function base64_url_encode($input) {
		return strtr(base64_encode($input), '+/=', '-_,');
	}

	function base64_url_decode($input) {
		return base64_decode(strtr($input, '-_,', '+/='));
	}
	
}

?>