﻿<?php
	$pageKeywords='software engineering, mobile applications testing, software testing, contact, Estonia, Tallinn';
	$pageTitle = 'γ-Test: Login';
	$pageDescription = 'User Login';

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'].'/gamma-test/';

include ($_SERVER['DOCUMENT_ROOT']."/header.php");
?>

<?php

//controller section
$error = '';
if (!empty($_POST) && isset($_POST)) {

    if ($_POST['doAction'] == 'login') {

        $fields = $_POST;

        $userObj = new User();
        if (!$userObj->userLogin($fields)) {
            $error = $userObj->error;
        }

    }
}

?>


		<!-- CONTENT -->

<div class="row pagecontent">
    <div class="content box col-md-12">

        <div class="row">
            <div class="col-md-6 col-sm-12">

                <form id="loginform" action="user_login.php" method="post" role="form">
                    <h2 class="cufon" align="left">Login</h2>
                    <?php
                     if(!empty($error)){
                         echo '<p style="color:red">'.$error.'</p>';
                     }
                    ?>

                    <div class="form-group">
                        <label for="Username">Username: <font color="red">*</font>:</label>
                        <input type="text" size="25" class="input-text form-control" id="username" name="username" required>
                    </div>

                    <div class="form-group">
                        <label for="Password">Password: <font color="red">*</font>:</label>
                        <input type="password" size="25" class="input-text form-control" id="password" name="password" required>
                    </div>

                    <input type="hidden" name="doAction" value="login"/>
                    <button type="submit" class="btn btn-default input-submit">Login</button>

                </form>

            </div>

        </div>

    </div>
</div>


	<?php 
	include ($_SERVER['DOCUMENT_ROOT']."/footer.php");
?>

</div> <!-- /main -->

</div> <!-- /bg -->

<!--<script type="text/javascript">-->
<!--   $(document).ready(function(){-->
<!--       Cufon.now();-->
<!--   });-->
<!---->
<!--</script>-->

</body>
</html>
