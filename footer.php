﻿<!-- FOOTER -->
<div class="row">
<div class="footer box col-md-12">

    <div class="f-left col-md-6" style="padding:10px 0">
        <p>Copyright &copy;&nbsp;2013-2016 <a href="/contact.php">Gamma Test</a></p>
    </div>
    <div style="clear: both"></div>
    <div class="f-left col-md-6 col-sm-12" style="padding: 0">

        <div class="col-sm-12 col-md-2" style="padding: 0">
            <div class="fb-like" data-href="http://www.gammatest.net" data-width="450"
                 data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
        </div>
        <div class="col-sm-12 col-md-2 col-md-offset-1" style="padding: 0">
            <!-- Place this tag where you want the widget to render. -->
            <div class="g-follow" data-annotation="bubble" data-height="20"
                 data-href="//plus.google.com/112167372274339457265" data-rel="author"></div>
        </div>
        <div class="col-sm-12 col-md-2" style="padding-left: 20px">
            <script src="//platform.linkedin.com/in.js" type="text/javascript">
                lang: en_EN
            </script>
            <script type="IN/Share" data-counter="right"></script>
        </div>


        <!--<table class="f-left">-->
            <!--<tr>-->
                <!--<td style="vertical-align: middle">-->
                    <!--<script src="//platform.linkedin.com/in.js" type="text/javascript">-->
                        <!--lang: en_EN-->
                    <!--</script>-->
                    <!--<script type="IN/Share" data-counter="right"></script>-->
                <!--</td>-->
                <!--<td style="vertical-align: middle">-->
                    <!--<div class="fb-like" data-href="http://www.gammatest.net" data-width="450"-->
                         <!--data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>-->
                <!--</td>-->
                <!--<td style="vertical-align: middle">-->
                    <!--&lt;!&ndash; Place this tag where you want the widget to render. &ndash;&gt;-->
                    <!--<div class="g-follow" data-annotation="bubble" data-height="20"-->
                         <!--data-href="//plus.google.com/112167372274339457265" data-rel="author"></div>-->
                <!--</td>-->
            <!--</tr>-->
        <!--</table>-->

    </div>


    <div class="f-right  col-md-6">
        <p class="f-right t-right">
            <strong>GAMMA TEST TECHNOLOGIES OÜ</strong><br>
            <strong>Aadress:</strong> 13920, Priisle tee 8-80, Tallinn, Estonia<br>
            <strong>Phone:</strong> +372 56 319 339<br>
            <strong>Email:</strong> <a href="mailto:info@gammatest.net">info@gammatest.net</a><br>
        </p>
    </div>

    <!-- Place this tag after the last widget tag. -->
    <script type="text/javascript">
        window.___gcfg = {lang: 'en'};

        (function () {
            var po = document.createElement('script');
            po.type = 'text/javascript';
            po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();
    </script>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-52351604-1', 'auto');
        ga('send', 'pageview');

    </script>

</div> <!-- /footer -->
</div>