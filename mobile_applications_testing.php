﻿<?php
$pageKeywords = 'mobile applications testing, android applications testing, ios applications testing, windows phone applications testing, mobile games testing, Estonia, Tallinn';
$pageTitle = 'γ-Test: Mobile applications testing';
$pageDescription = 'γ-Test - Functional and non-functional testing of mobile applcations and games';
include($_SERVER['DOCUMENT_ROOT'] . "/header.php");
?>

<div class="row pagecontent">
    <div class="content box col-md-12">
        <br>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="Mobile applications functional testing"/>Functional
                testing</h2>

            <p class="thumb"><img src="tmp/mobile_functional_testing.png" alt="Mobile applications functional testing"/>
            </p>
            <h5>Are the functional requirementes fully implemented by developers? Could the functionality be optimized or
                the number of screen-flows be reduced? Has a new functionality any conflicts with previous versions? <a
                    href='/contact.php'>Get in touch with γ-Test</a> and we'll pick functionality on the next level:
            </h5>
            </p>
        </div>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="Mobile applications compatibility testing"/>Compatibility
                testing</h2>

            <p class="thumb"><img src="tmp/compatibility_testing.jpg"
                                  alt="Mobile applications compatibility testing"/></a></p>
            <h5>Are you sure that your app is running on old devices as well as on the newest? Are you ready to lost old
                devices auditory for your app? <a href='/contact.php'>Ask us</a> and we help you to reach the maximum
                compatibilty of your application, providing a full compatibility testing</h5>
            <!--img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Complete verification on all possible devices</b><br>
           <img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Interoperability and installability testing on all possible platforms / OS</b><br-->
        </div>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="Mobile applications performance testing"/>Performance
                testing</h2>

            <p class="thumb"><img src="tmp/performance_testing.jpg" alt="Mobile applications performance testing"/></a>
            </p>
            <h5>How your application is working on older devices? What is the energy and memory consumption? Is your
                application having a memory leaks? <a href='/contact.php'>Ask us</a> and we will provide you a full
                picture of your app performance.</h5>
            <!--img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Mobile application testying on devices with different CPU's / different RAM amount</b></br>
            <img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Graphic performance testing for Game / 3D mobile applications</b></br>
            <img src="tmp/green_tick_small.jpg" alt="We test it" /><b>&nbsp;Network connecivity testing (server response time and packet loss) </b></br-->
        </div>

        <div class="clearfix"></div>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="Business applications testing"/>Business apps
                testing</h2>

            <p class="thumb"><img src="tmp/software_dev_support.jpg" alt="Business applications testing"/></a></p>
            <h5>Are the application fully satisfies customer business requirements? Do the personnel use your software
                in right way? <a href='/contact.php'>Ask us for a testing plan</a> and we will validate your software
                on the basis of business processes.</h5>
        </div>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="We test it"/>Mobile games testing</h2>

            <p class="thumb"><img src="tmp/mobile_games_testing.jpg" alt="Mobile games testing"/></a></p>
            <h5>Are you sure that your game is interesting for player? Are the player enough involved in your game from
                first seconds? Are you game enough balanced to be honest for all kinds of player? <a
                    href='/contact.php'>Contact us</a> and we make your game more exciting</h5>
        </div>

        <div class="col-md-4 col-sm-12">
            <h2 class="cufon"><img src="tmp/green_tick_50_50.jpg" alt="We test it"/>Localization testing</h2>

            <p class="thumb"><img src="tmp/localization_testing.png" alt="Localization testing"/></a></p>
            <h5>Are your app easily understandable on the target market? Are your taking into account such factors like
                formatting and cultural considerations? Whether you think about translation, localization or
                internationalization <a href='/contact.php'>ask our localization team</a> to help you conquer all
                possible markets</h5>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12">
            <h2 class="cufon" align="center">Are you not confident about mobile outsource testing? <a
                    href="/mobile_devices_rent.html">Check how we can help you providing mobile devices rent for
                    testing</a></h2>
        </div>

    </div>
    <!-- /content -->
</div>

<?php
include($_SERVER['DOCUMENT_ROOT'] . "/footer.php");
?>

</div> <!-- /main -->

</div> <!-- /bg -->

<!--<script type="text/javascript"> Cufon.now(); </script>-->

</body>
</html>
