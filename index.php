﻿<?php
$pageKeywords = 'software engineering, mobile applications testing, software testing, outsource software testing, desktop applications testing, web testing, Estonia, Tallinn';
$pageTitle = 'γ-Test: Providing quality to software';
$pageDescription = 'γ-Test - Providing quality to software: professional testing of mobile, web and desktop applications';

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'].'/gamma-test/';

include($_SERVER['DOCUMENT_ROOT'] . "/header.php");



?>

<!--<br>-->
<!-- SLIDER -->
<div class="row pagecontent">

    <div class="slider col-md-12">

        <div class="slider-container center-block">

            <div class="slider-slides center-block">

                <!-- SLIDE -->
                <div>
                    <div>

                        <h1 class="desc1">Mobile applications testing </h1>
                        <font class="desc hide-mobile">
                            <br><br>
                            Testing on old / no name devices <br>
                            Exploratory testing before release<br>
                            iOS / Android apps tesing<br>
                            Payment systems / wallets testing<br>
                            Business app testing <br>
                            Interoperability testing <br>
                            Installability testing <br>
                            Performance testing <br>
                            Security testing <br>
                            Mobile pages testing <br>
                            Games testing<br>
                            Localization testing<br>
                            Testing different screen resolutions <br>
                            Integrations testing <br>
                            Certification</font>
                        <img align="right" class="slideshowimg" src="tmp/donate-devices.png" alt="Testing on mobile devices"/>
                    </div>
                </div>

                <!-- SLIDE -->
                <div>
                    <div>

                        <h1 class="desc1">Desktop applications testing</h1>
                        <font class="desc hide-mobile"><br><br>
                            Application exploratory testing before release <br>
                            Compatibility testing <br>
                            Performance testing<br>
                            Games testing <br>
                            Localization testing <br>
                            Installability testing<br>
                            Business applications testing <br>
                            Security testing <br>
                            Testing on different monitors<br>
                            Integrations testing <br>
                            Certification
                        </font>
                        <img align="right"  class="slideshowimg" src="tmp/desktop_applications.png" alt="Desktop applications testing"/>


                    </div>
                </div>


                <div class="col-md-12 col-sm-12">
                    <div>

                        <h1 class="desc1">Web pages testing</h1>
                        <font class="desc hide-mobile"><br><br>
                            Functional testing<br>
                            Exploratory testing before release <br>
                            Payment systems / wallets testing <br>
                            Games testing <br>
                            Localization testing <br>
                            Business app testing <br>
                            Performance testing <br>
                            Security testing <br>
                            Interoperability testing <br>
                            Testing different screen resolutions <br>
                            Integrations testing
                        </font>
                        <img align="right"  class="slideshowimg" src="tmp/html_testing.png" alt="Web applications testing"/>
                    </div>
                </div>

                <!-- SLIDE -->


                <div class="col-md-12 col-sm-12">
                    <div>

                        <h1 class="desc1">Rent of mobile devices and developer toolkits</h1>
                        <font class="desc "><br><br>
                            Test your application on certain device or use mobile software developer toolkit to be sure
                            that app is running on all devices<br>
                            <table>
                                <tr class="hide-mobile">
                                    <td>Phones</td>
                                    <td>Tablets</td>
                                    <td>Devices toolkit for Android</td>
                                    <td>Devices toolkit for iOS</td>
                                </tr>
                                <tr>
                                    <td><img src="tmp/phones.jpg" alt="phones"/></td>
                                    <td><img src="tmp/tablets.jpg" alt="tablets"/></td>
                                    <td><img src="tmp/android_devices.jpg" alt="Android devices"/></td>
                                    <td><img src="tmp/iOSdevices.png" alt="iOS devices"/></td>
                                </tr>
                                <tr class="hide-mobile">
                                    <td>From 0.49€ / day</td>
                                    <td>From 1.67€ / day</td>
                                    <td>From 12.90€ / day</td>
                                    <td>From 13.10€ / day</td>
                                </tr>
                            </table>
                        </font>

                    </div>
                </div>

                <!-- SLIDE -->
                <div class="col-md-12 col-sm-12">
                    <div>

                        <h1 class="desc1" color="black">Support of software engineering</h1>
                        <font class="desc hide-mobile"><br><br>
                            Exploratory testing before release<br>
                            Requirements analysis <br>
                            Release testing <br>
                            Outsource testing <br>
                            Localization<br>
                            Test plan creation <br>
                            Errors analysis <br>
                            Test automation <br>
                        </font>
                        <img align="right"  class="slideshowimg" src="tmp/software_engineering.jpg" alt="Web applications testing"/>

                    </div>
                </div>


            </div>
            <!-- /slider-slides -->

        </div>
        <!-- /slider-container -->

        <ul class="slider-pagination center-block col-md-4 col-sm-4">
            <li><a href="#"><span>1</span></a></li>
            <li><a href="#"><span>2</span></a></li>
            <li><a href="#"><span>3</span></a></li>
            <li><a href="#"><span>4</span></a></li>
            <li><a href="#"><span>5</span></a></li>
        </ul>

    </div>
    <!-- /slider -->
</div>


<script type="text/javascript" charset="utf-8">
    $(function () {
        $('.slider').loopedSlider({
            container: ".slider-container",
            slides: ".slider-slides",
            pagination: "slider-pagination",
            containerClick: false,
            autoStart: 5000,
            slidespeed: 750,
            fadespeed: 500,
            addPagination: true
        });
    });
</script>

<!-- CONTENT -->
<div class="row pagecontent">
    <div class="content box col-md-12">

        <div class="col-md-4 col-sm-12">
            <h3 class="cufon">Software engineering support</h3>

            <p class="thumb"><a href="/software_engineering_support.php"><img
                        src="tmp/software_development_support.jpg" alt="Software development support"/></a></p>

            <p><h5>Are you release deadline too short? Are you looking for one-time outsource testing? Are you having
                problems with some part of software you developing? We provide you support in such areas as
                requirements analysis, test plan creation, software localization, complete testing before
                release</h5></p>

            <p class="more"><a href="/software_engineering_support.php">And even more...</a></p>
        </div>
        <div class="col-md-4 col-sm-12">
            <h3 class="cufon">Independent software testing</h3>

            <p class="thumb"><a href="/desktop_applications_testing.php"><img src="tmp/bug.png"
                                                                               alt="Independent software testing"/></a>
            </p>

            <p><h5>Are you doubting in quality of your software? Would you like to be confident that your customer will
                be satisfied with your application? Do you want that your mobile application will run on every device
without crash..<a href="/contact.php">Ask γ-Test</a> and we will perform an independing testing of your application...</h5></p>

            <p class="more"><a href="/desktop_applications_testing.php">And even more..</a></p>

        </div>
        <div class="col-md-4 col-sm-12">
            <h3 class="cufon">Rent device for testing</h3>

            <p class="thumb"><a href="mobile_devices_rent.php"><img src="tmp/enterprise-mobile-devices-20113.jpg"
                                                                     alt="Mobile devices rent for software development and testing"/></a>
            </p>

            <p><h5>Do you want to test your application yourself? You want to know how your application works on
                all devices? You do not want to buy a device for one-time testing? Especially for mobile
                application developers γ-Test provides a new service: <a href="/mobile_devices_rent.php">mobile devices
                    rent for software testing</a></h5>
            </p>

            <p class="more"><a href="/mobile_devices_rent.php">And even more...</a></p>

        </div>
    </div>
    <!-- /content -->
</div>

<?php

include($_SERVER['DOCUMENT_ROOT'] . "/footer.php");

?>


</div> <!-- /main -->


<!--<script type="text/javascript">-->
<!--    $(document).ready(function () {-->
<!--        Cufon.now();-->
<!--    });-->
<!--</script>-->










</body>
</html>
