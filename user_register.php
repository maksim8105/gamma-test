﻿<?php
	$pageKeywords='software engineering, mobile applications testing, software testing, contact, Estonia, Tallinn';
	$pageTitle = 'γ-Test: Registration';
	$pageDescription = 'Registration';

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'].'/gamma-test/';

	include ($_SERVER['DOCUMENT_ROOT']."/header.php");
?>

<?php

//controller section
$statusmsg = "";
if(!empty($_POST) && isset($_POST)){

    if($_POST['doAction'] == "registeruser"){

        $formdata = $_POST;

        //register user
        $userObj = new User();
        $userObj->saveUser($formdata);

        //send email
        $email = new Emails($formdata);
        $response = $email->sendRegisterConfirmEmail($formdata);

        if($response){
            //echo '<script>setTimeout(function(){ window.location.href = "'.SiteURL.'contact_confirmation.php"; }, 3000);</script>';
            $statusmsg = "Feedback succesfully sent!";
            //header('Location: /user_register_confirmation.php');

        }else{
            //echo '<script>setTimeout(function(){ window.location.href = "'.SiteURL.'error_contact_confirmation.php"; }, 3000);</script>';
            $statusmsg = "Error on Feedback send email!";
        }

        header('Location: /user_register_confirmation.php');


    }

}

?>

		<!-- CONTENT -->

<div class="row pagecontent">
    <div class="content box col-md-12">

        <div class="row">
            <div class="col-md-6 col-sm-12">

                <form id="registrationform" action="user_register.php" method="post" role="form">
                    <h2 class="cufon" align="left">Registration</h2>

                    <p class="error-msg" style="color:red;margin-top:5px; margin-bottom: 5px"></p>

                    <div class="form-group" style="padding-top: 10px">
                        <label for="Name">Your name <font color="red">*</font>:</label>
                        <input type="text" size="25" class="input-text form-control" id="name" name="name" required>
                    </div>

                    <div class="form-group">
                        <label for="Email">E-mail <font color="red">*</font>:</label>
                        <input type="text" size="25" class="input-text form-control" id="email" name="email" required>
                    </div>

                    <div class="form-group">
                        <label for="Username">Username <font color="red">*</font>:</label>
                        <input type="text" size="25" class="input-text form-control" id="username" name="username" required>
                    </div>

                    <div class="form-group">
                        <label for="Password">Password <font color="red">*</font>:</label>
                        <input type="password" size="25" class="input-text form-control" id="password" name="password" required>
                    </div>

                    <div class="form-group">
                        <label for="Confirmpassword">Confirm Password <font color="red">*</font>:</label>
                        <input type="password" size="25" class="input-text form-control" id="confirmpassword" name="confirmpassword" required>
                    </div>

                    <div class="form-group">
                        <label for="Company">Your company:</label>
                        <input type="text" size="25" class="input-text form-control" id="company" name="company" >
                    </div>

                    <div class="form-group">
                        <label for="Phone">Your phone <font color="red">*</font>:</label>
                        <input type="text" size="25" class="input-text form-control" id="phone" name="phone" required>
                    </div>

                    <div class="form-group">
                        <label for="Skype">Skype:</label>
                        <input type="text" size="25" class="input-text form-control" id="skype" name="skype">
                    </div>

                    <input type="hidden" name="doAction" value="registeruser"/>
                    <button type="submit" class="btn btn-default input-submit">Save</button>

                </form>

            </div>

        </div>

    </div>
</div>


	<?php 
	include ($_SERVER['DOCUMENT_ROOT']."/footer.php");
?>

</div> <!-- /main -->

</div> <!-- /bg -->

<script type="text/javascript">

    $(document).ready(function(){

        //validation
        var isValid = 1;

        //check email is already registered
        $("#email").focusout(function(){

            $(".error-msg").hide();

            var useremail = $(this).val();
            $.post( "controller.php",{uemail:useremail, doAction:'checkemail'}, function(data) {
                console.log(data);
                if(data.result == "1"){
                    $(".error-msg").text("This "+useremail+" email is already registered!");
                    $(".error-msg").fadeIn();
                    $(this).val("");
                    $("html, body").animate({ scrollTop: 0 }, "slow");

                    isValid = 0;

                }

            },'json');

        });

        //check username is already registered
        $("#username").focusout(function(){

            $(".error-msg").hide();

            var uusername = $(this).val();
            $.post( "controller.php",{uusername:uusername, doAction:'checkusername'}, function(data) {
                console.log(data);
                if(data.result == "1"){
                    $(".error-msg").text("This "+uusername+" username is already registered!");
                    $(".error-msg").fadeIn();
                    $(this).val("");
                    $("html, body").animate({ scrollTop: 0 }, "slow");

                    isValid = 0;

                }

            },'json');

        });


        $("#confirmpassword").focusout(function(){
            $(".error-msg").hide();
            var confirmpass = $(this).val();
           if(confirmpass != $("#password").val()){
               $(".error-msg").text("Password is not matched!");
               $(".error-msg").fadeIn();
               $(this).val("");
           }

        });

    });


</script>

</body>
</html>
