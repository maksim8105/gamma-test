<?php
$pageKeywords='software engineering, mobile applications testing, software testing, contact, Estonia, Tallinn';
$pageTitle = 'γ-Test: FeedBack';
$pageDescription = 'FeedBack';

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'].'/gamma-test/';

include ($_SERVER['DOCUMENT_ROOT']."/header.php");
?>

<?php

//controller section
$statusmsg = "";
if(!empty($_POST) && isset($_POST)){

    if($_POST['doAction'] == "feedback"){

        $formdata = $_POST;
        
        //Feedback save
        $feedbackObj = new Feedback();
        $feedbackObj->saveFeedback($formdata);



        if($response){
            //echo '<script>setTimeout(function(){ window.location.href = "'.SiteURL.'contact_confirmation.php"; }, 3000);</script>';
            $statusmsg = "Feedback succesfully sent!";
            //header('Location: /user_register_confirmation.php');

        }else{
            //echo '<script>setTimeout(function(){ window.location.href = "'.SiteURL.'error_contact_confirmation.php"; }, 3000);</script>';
            $statusmsg = "Error on Feedback send email!";
        }

        header('Location: /user_register_confirmation.php');


    }

}

?>

<!-- CONTENT -->

<div class="row pagecontent">
    <div class="content box col-md-12">

        <div class="row">
            <div class="col-md-6 col-sm-12">

                <form id="feedbackform" action="feedback.php" method="post" role="form">
                    <h2 class="cufon" align="left">FeedBack</h2>

                    <p class="error-msg" style="color:red;margin-top:5px; margin-bottom: 5px"></p>

                    <div class="form-group" style="padding-top: 10px">
                        <label for="Name">Your name <font color="red">*</font>:</label>
                        <input type="text" size="25" class="input-text form-control" id="name" name="name" required>
                    </div>

                    <div class="form-group">
                        <label for="Email">E-mail <font color="red">*</font>:</label>
                        <input type="text" size="25" class="input-text form-control" id="email" name="email" required>
                    </div>

                    <div class="form-group">
                        <label for="feedback">FeedBack <font color="red">*</font>:</label>
                        <textarea class="input-text form-control" id="feedback" name="feedback" rows="10" cols="70"></textarea>
                        <!--input type="text" size="25" class="input-text form-control" id="username" name="username" required-->
                    </div>

                    <input type="hidden" name="doAction" value="feedback"/>
                    <button type="submit" class="btn btn-default input-submit">POST</button>

                </form>

            </div>

        </div>

    </div>
</div>


<?php
include ($_SERVER['DOCUMENT_ROOT']."/footer.php");
?>

</div> <!-- /main -->

</div> <!-- /bg -->



</body>
</html>
