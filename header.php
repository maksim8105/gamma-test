﻿<?php

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'].'/gamma-test/';



require_once 'inc/common.inc.php';

if(isset($_GET['logout'])) {
    User::logOutUser();
}
//User::checkUserLogin();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>γ-Test - Providing quality to software</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="content-language" content="en"/>
    <meta name="description" content="<?php
		if ($pageTitle=='') {echo 'γ-Test - Providing quality to software: professional testing of mobile, web and desktop applications';} 
		else {echo $pageDescription;}
		?>
	"/>
    <meta name="keywords"
          content="mobile applications testing, websites testing, software testing, software quality, outsource testing, desktop applications testing, apps testing, quality assurance"/>
    <meta name="author" content="γ-Test: e-mail:info@gammatest.net"/>
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css"/>
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/skin.css"/>

    <script type="text/javascript" src="javascript/jquery.js"></script>
    <!--<script type="text/javascript" src="javascript/jquery.loopedslider.js"></script>-->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
    <!--<script type="text/javascript" src="javascript/cufon-yui.js"></script>
    <script type="text/javascript" src="javascript/font.font.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            Cufon.replace('.cufon, .nav, .slogan', {hover: true});
            <!--<link rel="stylesheet" type="text/css" href="/highslide/highslide.css" />
        });
    </script> -->

    <script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!--<link rel="stylesheet" type="text/css" href="/highslide/highslide.css" />-->
    <!--<script type="text/javascript" src="/highslide/highslide.js"></script>-->
    <!--<script type="text/javascript">-->
    <!--// override Highslide settings here-->
    <!--// instead of editing the highslide.js file-->
    <!--hs.graphicsDir = '/highslide/graphics/';-->
    <!--</script>-->

    <!--[if IE 6]>
    <script src="javascript/pngfix.js"></script>
    <script>DD_belatedPNG.fix('.logo img, .nav a, .slider, .slider-pagination a');</script><![endif]-->

    <!--Plug Bootstrap  -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
    <link href="css/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--<script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>-->

</head>


<body id="hp"> <!-- APPLY STYLE FOR HOMEPAGE -->
<div class="main container">
    <!-- HEADER -->
    <div class="row">
    <div class="header box col-md-12">
        <!--p class="logo"><a href="/index.html">
        <!-- SLOGAN -->
        <div class="slogan">
            <!--<div class="col-md-4">-->
            <!--<a href="/index.php" style="text-decoration:none;">-->
                <!--<h2 style="color:black"><i>γ-Test</i><br/></h2>-->
                <!--<h4 style="color:black"><i>Providing quality to software</i></h4></a>-->
            <!--</div>-->
            <div class="language"></div>
            <!-- /slogan -->

            </a></p-->
            <!-- NAVIGATION -->
            <!-- Fixed navbar -->
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header" style="padding: 5px;">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!--<a class="navbar-brand" href="#">Company Name</a>-->
                        <a href="/index.php" class="logolink">
                            <h2 style="color:black;"><i>γ-Test</i></h2>
                            <!--<h4 style="color:black"><i>Providing quality to software</i></h4>-->
                            <span style="color:black;"><i>Providing quality to software</i></span>
                        </a>
                    </div>
                    <div class="navbar-collapse collapse" >
                        <ul class="nav navbar-nav navbar-right">
<!--                            <li class="active"><a href="/.">Home</a></li>-->
                            <li><a href="/about.php">About</a></li>
                            <li><a href="/software_engineering_support.php">Software engineering</a></li>
                            <li><a href="/mobile_applications_testing.php">Mobile</a></li>
                            <li><a href="/desktop_applications_testing.php">Desktop</a></li>
                            <li><a href="/web_testing.php">Web</a></li>
                            <li><a href="/mobile_devices_rent.php">Rent of mobile devices</a></li>
                            <li><a href="/feedback.php">FeedBack</a></li>
                            <li><a href="/contact.php">Contact</a></li>
                            <?php
                            $username = User::getUserName();
                            if(!empty($username)){ ?>
                                <li><a href="#">Hi, <?php echo $username ?> </a></li>
                                <li><a href="?logout=true">Logout</a></li>
                            <?php }else{ ?>
                                <li><a href="#myModal1"  data-toggle="modal" data-target="#myModal1" >Sign In</a></li>
                                <li><a href="/user_register.php" >Sign Up</a></li>
                            <?php
                            }
                            ?>

                            <!--<li class="dropdown">-->
                                <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown">Themes <b class="caret"></b></a>-->
                                <!--<ul class="dropdown-menu">-->
                                    <!--<li class="dropdown-header">Admin & Dashboard</li>-->
                                    <!--<li><a href="#">Admin 1</a></li>-->
                                    <!--<li><a href="#">Admin 2</a></li>-->
                                    <!--<li class="divider"></li>-->
                                    <!--<li class="dropdown-header">Portfolio</li>-->
                                    <!--<li><a href="#">Portfolio 1</a></li>-->
                                    <!--<li><a href="#">Portfolio 2</a></li>-->
                                <!--</ul>-->
                            <!--</li>-->
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>



        </div>
        <!--<div style="position: absolute; left: 810px; top: 49px;">-->
        <!--&lt;!&ndash;/*$url=$_SERVER['PHP_SELF'];&ndash;&gt;-->
        <!--&lt;!&ndash;echo "<a href=\"".$url."\"><img src=\"image/flags/ee.png\" alt=\"Eesti keel\" title=\"Eesti keel\"></img></a>";&ndash;&gt;
        -->
        <!--&lt;!&ndash;echo "<a href='/ru".$url."'><img style="."'margin-left: 5px;'"." src='image/flags/ru.png' alt='Русский язык' title='Русский язык'></img></a>";&ndash;&gt;
        -->
        <!--&lt;!&ndash;*/?>&ndash;&gt;-->
        <!---->
        <!---->
        <!---->
        <!--</div>-->

    </div>
    </div>
    <!-- /header -->

    <!-- Bootstrap core JavaScript
            ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="css/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
    <script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.js"></script>
<!--    <script type="text/javascript" src="javascript/jquery.validate.js"></script>-->

    <script type="text/javascript">
        $(document).ready(function(){
            //form validations
            $("#loginform").validate();
            $("#registrationform").validate();
        });

    </script>


    <script type="text/javascript" src="javascript/jquery.js"></script>
    <script type="text/javascript" src="javascript/jquery.loopedslider.js"></script>




    <!-- Modal -->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Login</h4>
                </div>
                <div class="modal-body">
                    <form id="loginform" action="user_login.php" method="post" role="form">
                       
                        <?php
                        if(!empty($error)){
                            echo '<p style="color:red">'.$error.'</p>';
                        }
                        ?>

                        <div class="form-group">
                            <label for="Username">Username: <font color="red">*</font>:</label>
                            <input type="text" size="25" class="input-text form-control" id="username" name="username" required>
                        </div>

                        <div class="form-group">
                            <label for="Password">Password: <font color="red">*</font>:</label>
                            <input type="password" size="25" class="input-text form-control" id="password" name="password" required>
                        </div>

                        <input type="hidden" name="doAction" value="login"/>
                        <button type="submit" class="btn btn-default input-submit">Login</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
  